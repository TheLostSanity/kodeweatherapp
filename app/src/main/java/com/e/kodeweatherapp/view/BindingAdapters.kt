package com.e.kodeweatherapp.view

import android.view.View
import androidx.databinding.BindingAdapter
import com.e.kodeweatherapp.model.UiState

@BindingAdapter("app:uiStateLoading")
fun setVisibilityForLoadingElements(view: View, uiState: UiState) {
    view.visibility = when (uiState) {
        UiState.LOADING -> View.VISIBLE
        UiState.REFRESHING -> View.VISIBLE
        else -> View.GONE
    }
}

@BindingAdapter("app:uiStateData")
fun setVisibilityForDataElements(view: View, uiState: UiState) {
    view.visibility = when (uiState) {
        UiState.HAS_DATA -> View.VISIBLE
        UiState.REFRESHING -> View.VISIBLE
        UiState.NO_DATA -> View.GONE
        else -> View.GONE
    }
}

@BindingAdapter("app:uiStateError")
fun setVisibilityForErrorElements(view: View, uiState: UiState) {
    view.visibility = when (uiState) {
        UiState.ERROR -> View.VISIBLE
        else -> View.GONE
    }
}

@BindingAdapter("app:visibleGone")
fun View.visibleGone(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}
