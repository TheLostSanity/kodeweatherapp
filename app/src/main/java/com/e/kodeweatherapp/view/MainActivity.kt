package com.e.kodeweatherapp.view

import android.content.pm.PackageManager
import android.content.res.Resources.NotFoundException
import android.graphics.Outline
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.e.kodeweatherapp.R
import com.e.kodeweatherapp.databinding.ActivityMainBinding
import com.e.kodeweatherapp.viewmodel.MainViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        private const val DEFAULT_ZOOM = 15f
    }

    private val viewModel: MainViewModel by viewModel()
    private lateinit var geocoder: Geocoder
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var locationPermissionGranted = false
    private var locality = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.apply {
            lifecycleOwner = this@MainActivity
            model = viewModel
        }

        val curveRadius = 32F
        cvShowWeather.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view?.let {
                    outline?.setRoundRect(0, 0, it.width, (it.height + curveRadius).toInt(), curveRadius)
                }
            }
        }
        cvShowWeather.clipToOutline = true

        btnShowWeather.setOnClickListener { WeatherActivity.start(this, locality) }
        cvSearch.setOnClickListener { searchView.isIconified = false }

        (supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment).getMapAsync(this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(this, Locale.getDefault())

        viewModel.address.observe(this, androidx.lifecycle.Observer { address ->
            setBottomView(address)
        })
        viewModel.addressError.observe(this, androidx.lifecycle.Observer { error ->
            when (error) {
                is IOException -> Toast.makeText(this, getString(R.string.err_network), Toast.LENGTH_SHORT).show()
                is IllegalArgumentException -> Toast.makeText(
                    this,
                    getString(R.string.err_address_iae),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpMap()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
        }
    }

    private fun setUpMap() {
        getLocationPermission()
        try {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.google_maps_style))
        } catch (e: NotFoundException) {
            Toast.makeText(this, getString(R.string.err_map_style), Toast.LENGTH_SHORT).show()
        }
        mMap.setOnMapClickListener {
            mMap.clear()
            mMap.addMarker(
                MarkerOptions()
                    .position(it)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.place))
            )
            viewModel.requestAddress(geocoder, it.latitude, it.longitude)
        }
        viewModel.onMapSetUp()
    }

    private fun setBottomView(address: Address?) {
        if (address == null) {
            return
        }
        if (address.locality != null) {
            locality = address.locality
            if (cvShowWeather.visibility == View.GONE) {
                cvShowWeather.apply {
                    alpha = 0f
                    visibility = View.VISIBLE
                    animate().alpha(1f).setDuration(1000).setListener(null)
                }
            }
        } else {
            Toast.makeText(this, getString(R.string.no_locality_found), Toast.LENGTH_SHORT).show()
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            try {
                fusedLocationProviderClient.lastLocation.addOnSuccessListener(this) {
                    it?.let {
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(it.latitude, it.longitude),
                                DEFAULT_ZOOM
                            )
                        )
                    }
                }
            } catch (e: IllegalArgumentException) {
                Toast.makeText(this, getString(R.string.err_map_user_location), Toast.LENGTH_SHORT).show()
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }
}
