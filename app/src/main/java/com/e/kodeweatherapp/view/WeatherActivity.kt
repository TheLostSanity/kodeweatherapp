package com.e.kodeweatherapp.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.e.kodeweatherapp.R
import com.e.kodeweatherapp.databinding.ActivityWeatherBinding
import com.e.kodeweatherapp.model.OpenWeatherModel
import com.e.kodeweatherapp.model.UiState
import com.e.kodeweatherapp.model.WeatherCondition.*
import com.e.kodeweatherapp.viewmodel.WeatherDetailsViewModel
import kotlinx.android.synthetic.main.activity_weather.*
import org.koin.android.viewmodel.ext.android.viewModel

class WeatherActivity : AppCompatActivity() {

    companion object {
        const val INTENT_CITY = "CITY"
        fun start(context: Context?, city: String) = context?.startActivity(
            Intent(context, WeatherActivity::class.java).apply {
                putExtra(INTENT_CITY, city)
            }
        )
    }

    private lateinit var city: String
    private val viewModel: WeatherDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityWeatherBinding = DataBindingUtil.setContentView(this, R.layout.activity_weather)
        binding.apply {
            lifecycleOwner = this@WeatherActivity
            model = viewModel
        }

        vPullToRefresh.setOnRefreshListener {
            vPullToRefresh.isRefreshing = true
            viewModel.requestWeather(city)
        }

        setSupportActionBar(toolbar)
        city = intent.getStringExtra(INTENT_CITY)

        with(viewModel) {
            uiState.observe(this@WeatherActivity, Observer<UiState> { uiState ->
                when (uiState) {
                    UiState.HAS_DATA -> weather.value?.let { onWeatherChanged(it) }
                    UiState.ERROR -> {
                        if (error.value == null) {
                            Toast.makeText(this@WeatherActivity, R.string.err_network, Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this@WeatherActivity, R.string.err_server_returned, Toast.LENGTH_SHORT)
                                .show()
                        }
                        vPullToRefresh.isRefreshing = false
                    }
                }
            })
        }
        if (supportActionBar != null && supportActionBar?.title != city) {
            vPullToRefresh.isRefreshing = true
            viewModel.requestWeather(city)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = city
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun onWeatherChanged(weather: OpenWeatherModel) {
        Glide.with(this)
            .load(getString(R.string.image_url, weather.weather[0].icon))
            .into(ivIcon)
        vWeatherImage.setBackgroundResource(
            when (weather.weather[0].description) {
                RAIN.condition -> R.drawable.rain
                SNOW.condition -> R.drawable.snow
                MIST.condition -> R.drawable.mist
                CLEAR_SKY.condition -> R.drawable.clear_sky
                FEW_CLOUDS.condition -> R.drawable.few_clouds
                SHOWER_RAIN.condition -> R.drawable.shower_rain
                THUNDERSTORM.condition -> R.drawable.thunderstorm
                BROKEN_CLOUDS.condition -> R.drawable.broken_clouds
                SCATTERED_CLOUDS.condition -> R.drawable.scattered_clouds
                else -> R.color.colorPrimary
            }
        )
        vPullToRefresh.isRefreshing = false
    }
}
