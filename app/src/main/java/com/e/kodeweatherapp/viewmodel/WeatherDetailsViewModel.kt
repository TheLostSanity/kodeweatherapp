package com.e.kodeweatherapp.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.e.kodeweatherapp.model.OpenWeatherModel
import com.e.kodeweatherapp.model.Output
import com.e.kodeweatherapp.model.UiState
import com.e.kodeweatherapp.model.WeatherInteractor
import kotlin.math.roundToInt
import kotlin.math.truncate

class WeatherDetailsViewModel(private val weatherInteractor: WeatherInteractor) : ViewModel() {

    companion object {
        private const val ZERO_KELVIN = -273.15
        private const val ONE_HPA = 0.75006
        private const val PRECISION = 100
        private const val DEGREE360 = 360
        private const val WIND_DIRECTIONS_NUM = 16
        private const val DEGREE_NORMALIZER = DEGREE360 / WIND_DIRECTIONS_NUM / 2
    }

    private val _weather = MutableLiveData<OpenWeatherModel>()
    private val _error = MutableLiveData<Throwable?>()
    private val weatherInteractorOutputObserver = Observer<Output<OpenWeatherModel>> { output ->
        var weatherResult: OpenWeatherModel? = null
        when (output) {
            is Output.Success -> weatherResult = output.output
            is Output.Error -> {
                _error.value = output.exception
                _uiState.value = UiState.ERROR
            }
        }
        weatherResult?.let { updateWeatherValues(it) }
    }

    val weather: LiveData<OpenWeatherModel> = _weather
    val error: LiveData<Throwable?> = _error
    val temperature = ObservableField<String>()
    val weatherType = ObservableField<String>()
    val humidity = ObservableField<String>()
    val windDirectionIndex = ObservableField<Int>()
    val windSpeed = ObservableField<String>()
    val pressure = ObservableField<String>()

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState> = _uiState

    override fun onCleared() {
        super.onCleared()
        weatherInteractor.output.removeObserver(weatherInteractorOutputObserver)
    }

    fun requestWeather(cityName: String) {
        _uiState.value = when (_uiState.value) {
            UiState.HAS_DATA -> UiState.REFRESHING
            else -> UiState.LOADING
        }
        weatherInteractor.getWeatherByCity(cityName)
        weatherInteractor.output.observeForever(weatherInteractorOutputObserver)
    }

    private fun updateWeatherValues(weather: OpenWeatherModel) {
        _weather.value = weather
        temperature.set((weather.main.temp + ZERO_KELVIN).roundToInt().toString())
        weatherType.set(weather.weather[0].description.toUpperCase())
        humidity.set(weather.main.humidity.toString())
        windDirectionIndex.set(truncate((weather.wind.deg + DEGREE_NORMALIZER) % DEGREE360 / (DEGREE360 / WIND_DIRECTIONS_NUM)).toInt())
        windSpeed.set(weather.wind.speed.toString())
        pressure.set((truncate(weather.main.pressure * ONE_HPA * PRECISION) / PRECISION).toString())
        _uiState.value = UiState.HAS_DATA
    }
}