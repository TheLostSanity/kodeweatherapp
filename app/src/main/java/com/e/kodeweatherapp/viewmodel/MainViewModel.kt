package com.e.kodeweatherapp.viewmodel

import android.location.Address
import android.location.Geocoder
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.e.kodeweatherapp.model.LatLonMath
import com.e.kodeweatherapp.model.UiState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class MainViewModel(private val latLonMath: LatLonMath) : ViewModel() {

    private val _address = MutableLiveData<Address>()
    private val _addressError = MutableLiveData<Throwable>()

    val address: LiveData<Address> = _address
    val addressError: LiveData<Throwable> = _addressError

    val latLonInDms = ObservableField<Pair<List<String>, List<String>>>()
    val city = ObservableField<String>()
    val uiState = ObservableField(UiState.LOADING)
    val isSearchingLocality = ObservableField(false)

    fun onMapSetUp() {
        uiState.set(UiState.NO_DATA)
    }

    fun requestAddress(geocoder: Geocoder, lat: Double, lon: Double) {
        isSearchingLocality.set(true)
        if (uiState.get() != UiState.HAS_DATA) uiState.set(UiState.NO_DATA)
        var loc = emptyList<Address>()
        var err: Throwable? = null
        CoroutineScope(Dispatchers.IO).launch {
            try {
                loc = geocoder.getFromLocation(lat, lon, 1)
            } catch (e: IOException) {
                err = e
            } catch (e: IllegalArgumentException) {
                err = e
            }
            withContext(Dispatchers.Main) {
                _addressError.value = err
                _address.value =
                    if (loc.isNotEmpty())
                        loc[0]
                    else
                        null
                onRequestAddressResponse()
            }
        }
    }

    private fun onRequestAddressResponse() {
        if (!address.value?.locality.isNullOrEmpty()) {
            city.set(address.value?.locality)
            uiState.set(UiState.HAS_DATA)
        } else {
            uiState.set(UiState.ERROR)
        }
        if (address.value?.hasLatitude() == true) {
            address.value?.let {
                latLonInDms.set(
                    latLonMath.getLatLonInDms(
                        it.latitude,
                        it.longitude
                    )
                )
            }
        }
        isSearchingLocality.set(false)
    }
}