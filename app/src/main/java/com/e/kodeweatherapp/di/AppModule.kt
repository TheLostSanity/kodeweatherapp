package com.e.kodeweatherapp.di

import com.e.kodeweatherapp.model.*
import com.e.kodeweatherapp.viewmodel.MainViewModel
import com.e.kodeweatherapp.viewmodel.WeatherDetailsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { LatLonMath() }
    single<WeatherInteractor> { WeatherInteractorImpl(get()) }
    single<WeatherRepository> { WeatherRepositoryImpl(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { WeatherDetailsViewModel(get()) }
}