package com.e.kodeweatherapp.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    @GET("weather")
    fun getWeatherByCity(@Query("q") city: String): Call<OpenWeatherModel>
}