package com.e.kodeweatherapp.model

import retrofit2.Response

class WeatherRepositoryImpl(private val openWeatherApi: OpenWeatherApi) : WeatherRepository {
    override fun getWeatherByCity(city: String): Response<OpenWeatherModel> =
        openWeatherApi.getWeatherByCity(city).execute()
}