package com.e.kodeweatherapp.model

import retrofit2.Response

interface WeatherRepository {
    fun getWeatherByCity(city: String): Response<OpenWeatherModel>
}
