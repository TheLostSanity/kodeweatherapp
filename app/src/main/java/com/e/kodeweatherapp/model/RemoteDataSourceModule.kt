package com.e.kodeweatherapp.model

import com.e.kodeweatherapp.BuildConfig
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val remoteDataSourceModule = module {
    single { createRetrofit() }
}

private var client: OkHttpClient = OkHttpClient.Builder().addInterceptor { chain ->
    val original = chain.request()
    val originalHttpUrl = original.url()
    val url = originalHttpUrl.newBuilder()
        .addQueryParameter("appid", BuildConfig.WEATHER_API_KEY)
        .build()
    val requestBuilder = original.newBuilder()
        .url(url)
    val request = requestBuilder.build()
    chain.proceed(request)
}.build()

private val BASE_URL = "http://api.openweathermap.org/data/2.5/"
fun createRetrofit(): OpenWeatherApi = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .client(client)
    .addConverterFactory(GsonConverterFactory.create())
    .build()
    .create(OpenWeatherApi::class.java)