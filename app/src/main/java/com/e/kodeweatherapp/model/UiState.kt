package com.e.kodeweatherapp.model

enum class UiState {
    LOADING,
    REFRESHING,
    HAS_DATA,
    NO_DATA,
    ERROR
}