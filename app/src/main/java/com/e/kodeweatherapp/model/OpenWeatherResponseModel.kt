package com.e.kodeweatherapp.model

data class OpenWeatherModel(
    val main: Main,
    val name: String,
    val weather: List<Weather>,
    val wind: Wind
)

data class Wind(
    val deg: Double,
    val speed: Double
)

data class Weather(
    val description: String,
    val icon: String,
    val main: String
)

data class Main(
    val humidity: Double,
    val pressure: Double,
    val temp: Double
)
