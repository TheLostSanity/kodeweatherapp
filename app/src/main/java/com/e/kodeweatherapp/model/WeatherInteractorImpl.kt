package com.e.kodeweatherapp.model

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class WeatherInteractorImpl(private val weatherDataSourceImpl: WeatherRepository) : WeatherInteractor, CoroutineScope {

    override val output = MutableLiveData<Output<OpenWeatherModel>>()

    private val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun getWeatherByCity(cityName: String) {
        launch {
            val _output = try {
                val result = weatherDataSourceImpl.getWeatherByCity(cityName)
                if (result.isSuccessful) {
                    val body = result.body()
                    if (body != null) {
                        Output.Success(body)
                    } else {
                        Output.Error(null)
                    }
                } else {
                    Output.Error(Exception(result.errorBody()?.string() ?: "Network error"))
                }
            } catch (e: Exception) {
                Output.Error(null)
            }
            withContext(Dispatchers.Main) {
                output.value = _output
            }
        }
    }
}

