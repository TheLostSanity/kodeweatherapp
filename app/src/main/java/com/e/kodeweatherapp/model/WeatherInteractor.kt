package com.e.kodeweatherapp.model

import androidx.lifecycle.MutableLiveData

interface WeatherInteractor {
    val output: MutableLiveData<Output<OpenWeatherModel>>
    fun getWeatherByCity(cityName: String)
}