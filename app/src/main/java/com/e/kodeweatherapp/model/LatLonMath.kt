package com.e.kodeweatherapp.model

import kotlin.math.floor
import kotlin.math.truncate

class LatLonMath {

    fun getLatLonInDms(lat: Double, lon: Double) = Pair(convertToDms(lat), convertToDms(lon))

    private fun convertToDms(l: Double): List<String> {
        val degrees = floor(l).toInt()
        val minutes = floor(60 * (l - degrees)).toInt()
        val seconds = truncate((3600 * (l - degrees) - (60 * minutes)) * 100) / 100
        return listOf(degrees.toString(), minutes.toString(), seconds.toString())
    }
}