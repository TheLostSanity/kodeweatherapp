package com.e.kodeweatherapp.model

enum class WeatherCondition(val condition: String) {
    RAIN("rain"),
    SNOW("snow"),
    MIST("mist"),
    CLEAR_SKY("clear sky"),
    FEW_CLOUDS("few clouds"),
    SHOWER_RAIN("shower rain"),
    THUNDERSTORM("thunderstorm"),
    BROKEN_CLOUDS("broken clouds"),
    SCATTERED_CLOUDS("scattered clouds")
}